﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Rajat Sharma
// </reviewer>
//
// <copyright file="Extensions.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      Defines an extension Slice for array class.
// </summary>
// -----------------------------------------------------------------------

namespace Networking
{
    /// <summary>
    /// Add some functions to exsting structures.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Get the array slice between the two indexes.
        /// ... Inclusive for start index, exclusive for end index.
        /// </summary>
        /// <typeparam name="T">type of array</typeparam>
        /// <param name="source"> Source array </param>
        /// <param name="start">starting index</param>
        /// <param name="end">ending index</param>
        /// <returns>The Slice of array inclusive of start index only.</returns>
        public static T[] Slice<T>(this T[] source, int start, int end)
        {
            // Handles negative ends.
            if (end < 0)
            {
                end = source.Length + end;
            }

            int len = end - start;

            // Return new array.
            T[] res = new T[len];
            for (int i = 0; i < len; i++)
            {
                res[i] = source[i + start];
            }

            return res;
        }
    }
}