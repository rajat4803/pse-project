﻿//-----------------------------------------------------------------------
// <author> 
//     Parth Patel
// </author>
//
// <date> 
//    29-10-201
// </date>
// 
// <reviewer> 
//     Libin N George
// </reviewer>
// 
// <copyright file="CommunicationFactory.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This file implements singleton Communicator factory.
// </summary>
//---------------

namespace Networking
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Singleton Communicator Factory
    /// </summary>
    public static class CommunicationFactory
    {
        /// <summary>
        /// lock to make factory thread-safe
        /// </summary>
        private static readonly object Padlock = new object();

        /// <summary>
        /// Stores first communicator instance
        /// </summary>
        private static ICommunication communicator = null;

        /// <summary>
        /// To get communicator instance for server side
        /// </summary>
        /// <returns>communicator instance</returns>
        public static ICommunication GetCommunicator()
        {   // lock the resource to make it thread-safe
            lock (Padlock)
            {   
                if (communicator == null)
                {
                    communicator = new Communication();
                }
            }

            return communicator;
        }

        /// <summary>
        /// To get communicator instance for client side
        /// </summary>
        /// <param name="serverPort">port number where server is listening for client requests</param>        
        /// <returns>communicator instance</returns>
        public static ICommunication GetCommunicator(String serverPort)
        {   // lock the resource to make it thread-safe
            lock (Padlock)
            {
                if (communicator == null)
                {
                    communicator = new Communication(serverPort);
                }
            }

            return communicator;
        }
    }
}
