﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Rajat Sharma
// </reviewer>
//
// <copyright file="Helper.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This File contains Helper Functions for Data Outgoing/Ingoing Component.
// </summary>
// -----------------------------------------------------------------------

namespace Networking
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// Containts some helper functions needed by communication class.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Sometimes when we want to print the client, we print the endpoint corresponding to the client.
        /// </summary>
        /// <param name="tcpClient"> Input tcpClient </param>
        /// <returns>IP:Port corresponding to input tcpClient as String.</returns>
        public static string GetEndPointAddress(TcpClient tcpClient)
        {
            return ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
        }

        /// <summary>
        /// Sometimes when we want to print the socket, we print the endpoint corresponding to the socket.
        /// </summary>
        /// <param name="socket"> Input socket </param>
        /// <returns>IP:Port corresponding to input socket as String.</returns>
        public static string GetEndPointAddress(Socket socket)
        {
            return ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
        }

        /// <summary>
        /// Sometimes when we want to print the IP, we call this to get IP corresponding to the tcpClient.
        /// </summary>
        /// <param name="tcpClient"> Input tcpClient </param>
        /// <returns>IP corresponding to input tcpClient as String.</returns>
        public static string GetEndPoint(TcpClient tcpClient)
        {
            return ((IPEndPoint)tcpClient.Client.RemoteEndPoint).ToString();
        }

        /// <summary>
        /// Sometimes when we want to print the IP, we call this to get IP corresponding to the socket.
        /// </summary>
        /// <param name="socket"> Input socket </param>
        /// <returns>IP corresponding to input socket as String.</returns>
        public static string GetEndPoint(Socket socket)
        {
            return ((IPEndPoint)socket.RemoteEndPoint).ToString();
        }

        /// <summary>
        /// Combines two bytes array. 
        /// </summary>
        /// <param name="first">First Byte Array</param>
        /// <param name="second">Second Byte Array</param>
        /// <returns>Returns a combined byte array</returns>
        public static byte[] Combine(byte[] first, byte[] second)
        {
            byte[] ret = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }

        /// <summary>
        /// Return byte array corresponding to an integer in BigEndian format. 
        /// </summary>
        /// <param name="val"> Input Integer</param>
        /// <returns>byte conversion corresponding to an iteger.</returns>
        public static byte[] GetBytes(int val)
        {
            byte[] intAsBytes = BitConverter.GetBytes(val);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(intAsBytes);
            }

            return intAsBytes;
        }
    }
}