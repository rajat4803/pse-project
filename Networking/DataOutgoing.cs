﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Rajat Sharma
// </reviewer>
//
// <copyright file="DataOutgoing.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This File contains the Data Outgoing Component.
//      It checks for data in queue by polling.If queue has a message, send it to its designation.
//      This file is a part of Networking Module
// </summary>
// -----------------------------------------------------------------------

namespace Networking
{
    using System;
    using System.Collections;
    using System.Net;
    using System.Net.Sockets;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;

    /// <summary>
    /// Part of communication class which implements the DataOutgoing Component and is tight coupled with DataIncoming Component.
    /// </summary>
    public partial class Communication : ICommunication
    {
        /// <summary>
        /// Used as main lock for lockObjects,lockStatus,ConnectedClients to insert remove entries.
        /// </summary>
        private readonly object mainLock = new object();

        /// <summary>
        /// In case of professors machine it store's localip, in case of student it stores professor's ip.
        /// </summary>
        private string ipAddress;
        
        /// <summary>
        /// In case of professors machine it store's listening port, in case of student it stores professor's port.
        /// </summary>
        private int port; 
        
        /// <summary>
        /// Tells if the professor's listening socket/student's connection to professor is up/down.
        /// </summary>
        private bool isRunning = false;
        
        /// <summary>
        /// Stores the listening socket for professor, nothing for student.
        /// </summary>
        private TcpListener serverSocket;

        /// <summary>
        /// No of times to resend a message if acknowledgement is not received.
        /// </summary>
        private int maxRetries;
        
        /// <summary>
        /// Time to wait for recipient to send acknowledgement.
        /// </summary>
        private int waitTimeForAcknowledge;
        
        /// <summary>
        /// Local Variable to Store if this module is instantiated as professor or student.
        /// </summary>
        private bool isStudent = false;
        
        /// <summary>
        /// Stores sockets of clients connected to this machine.
        /// </summary>
        private Hashtable connectedClients;

        /// <summary>
        /// MD5 message+IP,bool - represents if the acknowledgement of a message is received or inwait.
        /// </summary>
        private Hashtable acknowledgeStatus;

        /// <summary>
        /// IP nd bool - gives the status of a lock true - Some thread has acquired the lock _ false - lock is free.
        /// </summary>
        private Hashtable lockStatus;

        /// <summary>
        /// IP nd object - Contains the object to be used as reference to lock.
        /// </summary>
        private Hashtable lockObjects;

        /// <summary>
        /// Reference to thread which runs the a function to pop data from queue and process it.
        /// </summary>
        private Thread sendingThread;
        
        /// <summary>
        /// Reference to thread which handles the receiving end of the socket.(different function on professor and studnet).
        /// </summary>
        private Thread receivingThread;

        /// <summary>
        /// Initializes a new instance of the <see cref="Communication"/> class. For Professor Machine.
        /// </summary>
        /// <param name="port">Port to start the listening server on</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement.</param>
        public Communication(int port, int maxRetries, int waitTimeForAcknowledge)
        {
            Communication.T0("Communication Object Created On Professor Machine");
            this.port = port;
            this.maxRetries = maxRetries;
            this.waitTimeForAcknowledge = waitTimeForAcknowledge * 10;

            this.connectedClients = new Hashtable(); // Key = IP , Socket
            this.acknowledgeStatus = new Hashtable(); // Key =  MESSAGEEHASH + IP //TODO 
            this.lockStatus = new Hashtable(); // Key =  IP , Bool
            this.lockObjects = new Hashtable(); // Key = IP, Obj
        }

        /// <summary>
        /// /// Initializes a new instance of the <see cref="Communication"/> class. For student machine.
        /// </summary>
        /// <param name="ipAddress">IP address of professor's machine.</param>
        /// <param name="port">Port of professor's machine</param>
        /// <param name="maxRetries">No of times to resend a message if acknowledgement is not received.</param>
        /// <param name="waitTimeForAcknowledge">Time to wait for recipient to send acknowledgement.</param>
        public Communication(string ipAddress, int port, int maxRetries, int waitTimeForAcknowledge)
        {
            Communication.T0("Communication Object Created On Student Machine");
            this.ipAddress = ipAddress;
            this.isStudent = true;
            this.port = port;
            this.maxRetries = maxRetries;
            this.waitTimeForAcknowledge = waitTimeForAcknowledge * 10;

            this.connectedClients = new Hashtable(); // Key = IP , Socket
            this.acknowledgeStatus = new Hashtable(); // Key =  MESSAGEEHASH + IP //TODO 
            this.lockStatus = new Hashtable(); // key =  IP , Bool
            this.lockObjects = new Hashtable(); // key = IP, Obj
        }

        /// <summary>
        /// Logger Level zero
        /// </summary>
        /// <param name="ab">string to log</param>
        public static void T0(string ab)
        {
            Console.Write("Log:::" + ab + "\n");
        }

        /// <summary>
        /// Logger Level one
        /// </summary>
        /// <param name="ab">string to log</param>
        public static void T1(string ab)
        {
            Console.Write("----------------------------------------------------------------------------\nLog:::" + ab + "\n----------------------------------------------------------------------------\n");
        }

        /// <summary>
        /// Logger level two
        /// </summary>
        /// <param name="ab">string to log</param>
        public static void T2(string ab)
        {
            Console.Write("Log:::" + ab + "\n");
        }

        /// <summary>
        /// Starts the receiving and sending threads of the module.
        /// </summary>
        /// <returns> true if there were no problems with initialization(like port is free.) otherwise false. </returns>
        public bool Start()
        {
            if (!this.isStudent)
            {
                try
                {
                    this.receivingThread = this.StartReceiver();
                    this.receivingThread.Start();
                }
                catch
                {
                    Communication.T0(string.Format("Tried Initializing on Port {0} which wasn't free", this.port));
                    return false;
                }
            }

            this.isRunning = true;
            this.sendingThread = new Thread(this.SendFromQueue);
            this.sendingThread.Start();
            return this.isRunning;
        }

        /// <summary>
        /// Stops all the sockets and main threads.
        /// </summary>
        public void Stop()
        {
            this.isRunning = false;
            if (this.serverSocket != null)
            {
                this.serverSocket.Stop();
            }
            
            // Close all clients sockets to terminate their threads
            foreach (TcpClient client in this.connectedClients.Values)
            {
                client.Close();
            }

            this.sendingThread.Abort();
            this.receivingThread.Abort();
        }

        /// <summary>
        /// Sends a given message by formatting it.
        /// </summary>
        /// <param name="message">The message to be sent</param>
        /// <param name="retries">Tells which retry is this to send the message</param>
        /// <param name="socket">The socket to which send the data on</param>
        /// <param name="isAck">Tells if the message being sent is acknowledgment.</param>
        private void SendHelper(string message, int retries, Socket socket, int isAck)
        {
            ClientState currentState = new ClientState
            {
                Socket = socket
            };
            if (currentState.IP == "0.0.0.0")
            {
                DataStatusNotify(message, StatusCode.Failure);
                return;
            }

            Communication.T0("SendHelper Called. EndPoint" + Helper.GetEndPoint(socket));

            byte[] byteData = Encoding.ASCII.GetBytes(message);
            byte[] lengthBytes = Helper.GetBytes(byteData.Length);
            byte[] ackBits = new byte[] { (byte)isAck };

            // Every message sent over network has format (prefix = length + ackbit) + messagebytes(messagebytes.length=length)
            byte[] prefix = Helper.Combine(lengthBytes, ackBits);

            currentState.Retries = retries;
            currentState.DataSent = 0;
            currentState.Message = message;
            currentState.SetDataToSend(Helper.Combine(prefix, byteData));

            Communication.T0("Waiting To Acquire Lock IP EndPoint" + Helper.GetEndPoint(socket));

            // We maintain locks per socket so that more than one thread don't send at the same time on same socket, otherwise data would overlap.
            while (!this.GetLock(currentState.IP))
            {
            }

            Communication.T0("Lock Acquired. Calling BeginSend EndPoint" + Helper.GetEndPoint(socket));
            socket.BeginSend(currentState.DataToSend(), 0, currentState.DataToSend().Length, 0, new AsyncCallback(this.SendCallback), currentState);
            Communication.T0("Returned EndPoint" + Helper.GetEndPoint(socket));
        }

        /// <summary>
        /// Runs a infinity loop which keeps checking the queue at regular intervals,
        /// if there are any messages in the queue, it sends them.
        /// </summary>
        private void SendFromQueue()
        {
            Socket clientSocket;
            while (true)
            {
                /* TryDequeue returns false if dequeue was unsuccessful 
                 * If successful it returns true and stores result in currentRequest.
                 */
                if (!SendRequestQueue.TryDequeue(out SendRequest currentRequest))
                {
                    Communication.T0("Unable To Dequeue. Going to Sleep");
                    Thread.Sleep(1000);
                    continue;
                }

                /* Below conditions results in false if:
                 * there isn't any existing connection to target.
                 * In case of professor we fail, In case of student we start a connection.
                 */
                if (this.connectedClients.ContainsKey(currentRequest.TargetIPAddress.ToString()))
                {
                    clientSocket = (Socket)this.connectedClients[currentRequest.TargetIPAddress.ToString()];
                    Communication.T0("Connection For EndPoint " + Helper.GetEndPoint(clientSocket) + " Found.");
                    this.SendHelper(currentRequest.Data, 0, clientSocket, 0);
                }
                else
                {
                    if (this.isStudent)
                    {
                        if (currentRequest.TargetIPAddress.ToString() != this.ipAddress)
                        {
                            Communication.T0("The Professor Device Ip Address Doesn't Match with Target Ip Address.");
                            throw new InvalidOperationException();
                        }

                        if (this.InitiateConnection())
                        {
                            Communication.T0("Connection Initiated to Prof. at IP:" + this.ipAddress + " Port:" + this.port + "\n");
                            this.SendHelper(currentRequest.Data, 0, (Socket)this.connectedClients[currentRequest.TargetIPAddress.ToString()], 0);
                            continue;
                        }
                    }
                    
                    DataStatusNotify(currentRequest.Data, StatusCode.Failure); // failure callback
                    continue;
                }
            }
        }

        /// <summary>
        /// Callback for asynchronous send, if data is left to send, then send the remaining data, otherwise gointo wait for acknowledgement state.
        /// </summary>
        /// <param name="ar">An IAsyncResult that references the asynchronous send.</param>
        private void SendCallback(IAsyncResult ar)
        {
            ClientState state = (ClientState)ar.AsyncState;
            int sentData = state.Socket.EndSend(ar, out SocketError socketError);

            Communication.T0(string.Format("BytesSent : {0}   TotalBytes: {1}", sentData, state.DataToSend().Length));

            if (socketError != SocketError.Success)
            {
                Communication.T0("SocketError EndPoint :: " + Helper.GetEndPoint(state.Socket));  // Socket Failed
               
                // Remove any lockobjects,sockets,status bit associated to failed socket.
                this.ResetClient(state.IP);
                DataStatusNotify(state.Message, StatusCode.Failure);
                if (this.isStudent)
                {
                    this.isRunning = false;
                }

                return;
            }

            state.DataSent += sentData;

            if (state.DataSent != state.DataToSend().Length)
            {   // not all data was sent
                Communication.T0("Sending Remaining DataBytes(Calling BeginSend) :: " + (state.DataToSend().Length - sentData));
                state.Socket.BeginSend(
                             state.DataToSend(), state.DataSent, state.DataToSend().Length - state.DataSent, SocketFlags.None, new AsyncCallback(this.SendCallback), state);
            }
            else
            {
                Communication.T0(string.Format("EndPoint {0} Message Sent :: {1}", Helper.GetEndPoint(state.Socket), state.Message));
                
                // Now that all data has been sent we release the lock.
                this.ReleaseLock(state.IP);
                if (state.DataToSend()[4] == 0)
                {
                    this.WaitForAcknowledgement(state.IP, state.Message, state.Retries + 1);
                }
            }
        }

        /// <summary>
        /// Goes into wait for acknowledgment for a given message,ip 
        /// If acknowledgement is received within given time it calls DataStatusNotify
        /// otherwise it retries.
        /// if maxretries are exceeded it calls DataStatusNotify with failure.
        /// </summary>
        /// <param name="ip">The Ip Corresponding to the client it is waiting for.</param>
        /// <param name="message">Message For which it is waiting</param>
        /// <param name="retries">How many tries have already been made to send above data</param>
        private void WaitForAcknowledgement(string ip, string message, int retries)
        {
            string key = this.MD5Hash(message) + ip;
            this.acknowledgeStatus[key] = false;

            Communication.T0("Message : " + message + "\n\t Key : " + key);
            Communication.T0("Sleeping Thread; Message: " + message + "IP : " + ip);
            Thread.Sleep(this.waitTimeForAcknowledge);
            Communication.T0("Waking Thread; Message: " + message + "IP : " + ip);

            object ackStatus = this.acknowledgeStatus[key];

            if (ackStatus is bool)
            {
                if ((bool)ackStatus == true)
                {
                    this.acknowledgeStatus.Remove(key);
                    DataStatusNotify(message, StatusCode.Success);
                    return;
                }
            }
            else
            {
                Communication.T0("AcknowledgeStatus didn't have bool");
                throw new InvalidOperationException();
            }

            Communication.T0("Not Received Acknowledgement");
            Socket refreshSocket = (Socket)this.connectedClients[ip];
            this.SendHelper(message, retries + 1, refreshSocket, 1);
        }

        /// <summary>
        /// Starts a connection from student's machine to professor
        /// </summary>
        /// <returns> true if successful connection was made otherwise false.</returns>
        private bool InitiateConnection()
        {
            Communication.T0(string.Format("Creating connection to professor IP {0} Port {1}", this.ipAddress, this.port));

            TcpClient tcpClient = new TcpClient();
            tcpClient.Connect(this.ipAddress, this.port);
            Socket newSocket = tcpClient.Client;
            this.receivingThread = new Thread(() => this.HandleClientRequest(tcpClient));
           
            // Below method polls a socket to tell if it's writable.
            if (newSocket.Poll(-1, SelectMode.SelectWrite))
            {
                Communication.T0(string.Format("This Socket {0} is writable.", Helper.GetEndPoint(newSocket)));
                this.receivingThread.Start();
                if (!this.lockObjects.ContainsKey(this.ipAddress))
                {
                    this.lockObjects[this.ipAddress] = new object();
                }

                if (!this.lockStatus.ContainsKey(this.ipAddress))
                {
                    this.lockStatus[this.ipAddress] = false;
                }

                this.connectedClients[this.ipAddress] = newSocket;
                this.isRunning = true;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Remove Locks,Status and objects corresponding to an ip
        /// </summary>
        /// <param name="ip">Ip for which locks are to be removed.</param>
        private void ResetClient(string ip)
        {
            lock (this.mainLock)
            {
                if (!((Socket)this.connectedClients[ip]).Connected)
                {
                    this.connectedClients.Remove(ip);
                    this.ReleaseLock(ip);
                    this.lockStatus.Remove(ip);
                    this.lockObjects.Remove(ip);
                }
                else
                {
                    this.ReleaseLock(ip);
                }
            }
        }

        /// <summary>
        /// Grants a Lock to a thread . There exists 1 lock/ip.
        /// </summary>
        /// <param name="ip">Ip for which lock is needed.</param>
        /// <returns>true if lock is granted.</returns>
        private bool GetLock(string ip)
        {
            try
            {
                if ((bool)this.lockStatus[ip] != true)
                {
                    lock (this.lockObjects[ip])
                    {
                        if ((bool)this.lockStatus[ip] != true)
                        {
                            this.lockStatus[ip] = true;
                            return true;
                        }
                    }
                }
            }
            catch
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Release he lock corresponding to an ip.
        /// </summary>
        /// <param name="ip">Ip for which lock is to be released</param>
        private void ReleaseLock(string ip)
        {
            if (this.lockStatus.ContainsKey(ip))
            {
                this.lockStatus[ip] = false;
            }
        }

        /// <summary>
        /// Updates the acknowledgeStatus table to tell that an acknowledgement message has been received.
        /// </summary>
        /// <param name="key">Key corresponding to the message to be acknowledged.</param>
        private void Acknowledge(string key)
        {
            Communication.T0("Received Acknowledgement for HASH " + key);
            if (this.acknowledgeStatus.ContainsKey(key))
            {
                Communication.T0("Received Acknowledgement for HASH " + key + " was valid ");
                this.acknowledgeStatus[key] = true;
            }
            else
            {
                this.waitTimeForAcknowledge += 1;
            }
        }

        /// <summary>
        /// Returns an MD5Hash of a string.
        /// A message's acknowledgement is sent by sending an MD5hash of that message
        /// </summary>
        /// <param name="input">String For which MD5 hash has to be generated.</param>
        /// <returns>string containing MD5hash of input string</returns>
        private string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }

            return hash.ToString();
        }
    }
}