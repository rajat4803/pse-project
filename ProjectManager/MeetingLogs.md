# Meeting Logs

_last updated: Sept 13, 2015_

## Meeting 0: (Aug 25, 2018)
- Proposed various architectural implementations. Finally decided to go with Dependency based model.
- Decided that we would do a Peer to Peer system, because it's more abstract and generalizable. 
### Pull based Autonomous modules
- Every module constantly listens to its state. A change in state triggers an action associated with that module. 
- This demolishes dependency of a module on any other module. "Autonomous".
- Not feasible as you would have 7 executables running on every machine. 
- Inherently, it's the same as Dependency based implementation. 

### Centralised control
- UI is the only thing that calls every other module, no module can depend on any other module.
- Unnecessary load on UI. Avoidable centralization

### Dependency based modules
- Each module will provide library functions to be used by different modules. 
- Most obvious solution and easy to implement. 

## Meeting 1: (Aug 29, 2018)
- Proposed various ways to imitate client server in a peer to peer network.
- Decided that we would do a client server model, because of the specific requirements and complexities in imitating. 
### Electing the Server
- Members elect the server and Server's UI shows a toggle Test mode switch which broadcasts a "TEST MODE ON" message to the network. 
- When toggled, client UI respond to the broadcast and turn of sending messages to peers. 
- Here the assumption is that the peers elect the professor as the server.
- What if user modifies UI and uses the mofified UI.
### Submit Public and Private keys to the Server
- Peers share their private and public keys with the teacher. Now when Test mode is On, and if students try to talk, the 
teacher would be able to decrypt the message and know who is the intruder and kick him out of the network. 
- Incentive for not cheating here is not being caught. 

## Meeting 2: (Sept 08, 2018)
- Doubts clearing. 
- Deciding persistence design. 
- Networking: Singleton implementation. 
- Decided that all the reports will be in Markdown. 
- Test Harness and modules discussion. 