﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchemaTeam
{
    public interface ISchema
    {
        IDictionary<string, string> Decode(string data, bool partialDecoding);
        string Encode(Dictionary<string, string> tagDict);
    }
}
